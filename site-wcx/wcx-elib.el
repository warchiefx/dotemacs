;;; wcx-elib.el --- Loading Elib
;; 
;; Filename: wcx-elib.el
;; Description: 
;; Author: Ricardo Restituyo
;; Email: warchiefx@gmail.com
;; Created: Sun Oct 26 20:27:31 2008
;; 
;; Last-Updated: Sun Oct 26 20:28:58 2008 (-14400 AST)
;;           By: Ricardo Restituyo
;; Version: $Id$
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(add-to-list 'load-path (concat site-lisp-path "elib"))

(provide 'wcx-elib)